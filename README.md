# Hyper Remarketing

It's a script to manage **Adwords dynamic remarketing** in Planet 3 and other sites. Please note it's advisable to know some JavaScript to configure it and use it in your own site.

It uses Adwords Audiences (inside Shared Libraries). It works both with `google_conversion_label` and `google_tag_params`.

It's configurable by editing the file `spanishRemarketing.js` and extendable by extending the `hyperRemarketing` object, or even your own object. For example using: 

```javascript
var petitionPagesRemarketing = Object.create(spanishRemarketing);
```

## Download

Use git to download this script:

```bash
git clone https://bitbucket.org/greenpeace/hyperremarketing.git
```

### Test in a local server

To test your script in your computer you'll need to use the files in a local server like MAMP or WAMP.

You'll also need to install **jQuery** and gulp locally. With [npm](https://nodejs.org/) just do: 

```bash
cd path/to/hyperRemarketing
npm install
```

### Concatenate and minify js

It uses [Gulp](http://gulpjs.com/) to concatenate and minify JavaScript. After installing Gulp globally and the dependencies locally just run:

```bash
gulp
```

It will create the file `remarketing.min.js` that you should upload and link to from the footer your pages.

## Configure

### Your Adwords shared library

Before configuring Hyper Remarketing you must **create audiences and labels in your Adwords shared library** and take notes on the code for each label.

### Edit the config file

Configure your account and audiences by editing `spanishRemarketing.js`

### Test

Test by editing the pages in the test directory and use [Google Tag Assistant](https://chrome.google.com/webstore/detail/tag-assistant-by-google/kejbdjndbnbjgmefkgdddjlbokphdefk?hl=en) to check they work as espected.

## Use

Concatenate and minify the JavaScript files with **gulp** as described above.

**Important**: Hyper Remarketing requires jQuery to be loaded before `remarketing.min.js`. Make sure your pages use it.

### Upload to Google Storage

Use Google SDK **gsutil** to upload your `remarketing.min.js` (and `track.js` for the GP Magazine) to Google Clould Storage (CDN).

```bash
cd path/to/your/local/hyperRemarketing
gsutil defacl set public-read gs://hyperremarketing
gsutil cp -z js remarketing.min.js track.js gs://hyperremarketing
gsutil setmeta -h "Cache-Control:max-age= 604800" gs://hyperremarketing/*.js
```

(Replace **hyperremarketing** by your Google Storage bucket name.)

Instead of Google Cloud Storage you can use Planet 3 or any https server where you can upload static files.

### In Planet 3

You'll need to add your `remarketing.min.js` to:

1. Your **Planet 3 home page** desktop (inheritable)
2. Your **blog template** (mobile and desktop)
3. Your **mobile site** (There's a bug/cache problem, it will need a try-and-error approach)
4. Any **raw html pages** you have in your site (like petitions)

Please note P3 mobile pages don't have content tags, all your mobile visitors will be tagged with the same content label.



### Html to add:

Just add, before the closing `</body>` tag:

```html
	<script src = "/path/to/your/hyperRemarketing/file/remarketing.min.js"></script>
	<script>
		spanishRemarketing.track();
	</script>
```

`track.js` is a file to use in GP magazine pages, where you can add javascript from external pages only.

