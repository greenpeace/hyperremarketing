/* global hyperRemarketing */

var spanishRemarketing = Object.create(hyperRemarketing);

spanishRemarketing.GoogleConversionId = 1053230267; // Remarketing ID cuenta webmaster

spanishRemarketing.GoogleOtherContentLabel = 'fIj0CJ7ZpWoQu4mc9gM'; // Vista otra pagina (HR)

spanishRemarketing.GoogleOtherActionLabel = 'LuFFCP_-pWoQu4mc9gM'; // Otras acciones de usuario (HR)

spanishRemarketing.version = '20170105.1';

/**
 * Configure the content related labels
 */
spanishRemarketing.configContentTagsLabels = [{
    label: 'vUwHCIqOnmoQu4mc9gM', // Visita pagina de herencias (HR)
    pageTags: ['herencias']
}, {
    label: 'OhZECJrntmoQu4mc9gM', // Visita página de voluntariado (HR)
    pageTags: ['voluntarios', 'red de voluntariado', 'participación']
}, {
    label: 'NIbTCJLNsmoQu4mc9gM', // Visita página sobre Ártico (HR)
    pageTags: ['artico', 'ártico', 'expedición al ártico', 'prospecciones petrolíferas en el ártico']
}, {
    label: 'cdl4CKTNsmoQu4mc9gM', // Visita página sobre abejas (HR)
    pageTags: ['abejas', 'abeja', 'agricultura ecológica']
}, {
    label: 'PXyiCI-QnmoQu4mc9gM', // Visita página sobre plásticos (HR)
    pageTags: ['plásticos', 'plástico', 'botellas de plástico', 'microplásticos']
}, {
    label: 'A-_TCKqQnmoQu4mc9gM', // Visita página sobre pescado (HR)
    pageTags: ['pesca sostenible', 'pesca artesanal', 'reforma a la ppc', 'política pesquera', 'sobrepesca', 'descartes']
}, {
    label: '8YVQCM6TnmoQu4mc9gM', // Visita página sobre agricultura / transgénicos (HR)
    pageTags: ['transgénicos', 'inginería genética', 'organismos modificados genéticamente', 'agricultura']
}, {
    label: 'FwijCNaVnmoQu4mc9gM', // Visita página sobre ONG Greenpeace (HR)
    pageTags: ['quiénes somos y participación', 'greenpeace', 'cómo nos organizamos']
}, {
    label: 'wkZ5CNPQ5GoQu4mc9gM', //  Visita pagina de TTIP (HR)
    pageTags: ['ttip', 'acuerdo transatlántico']
}, {
    label: 'DW-DCO2XnmoQu4mc9gM', // Visita página sobre cambio climático (HR)
    pageTags: ['energía', 'renovables', 'renovables', 'energía renovable', 'revolución energética', 'iberdrola', 'reforma energética', 'bajate la potencia', 'energia 3.0', 'cambio climático']
}, {
    label: 'n88_CJ_vtmoQu4mc9gM', // Visita página sobre bosques (HR)
    pageTags: ['bosques', 'bosque primario', 'bosques tropicales', 'amazonia', 'amazonas', 'indonesia', 'incendios en indonesia', 'incendios', 'incendios forestales', 'eucaliptos', 'eucalipto', 'plantaciones de eucalipto', 'florestal']
}];


/**
 * Configure the traffic source related labels
 */
spanishRemarketing.configTrafficSourceLabels = [{
    label: 'TeGeCOHatmoQu4mc9gM', // Click en newsletter a leads (HR)
    membertype: 'lead',
    utm_source: ['newsletter-leads'],
    utm_medium: ['email']
}, {
    label: 'bhNJCLOKnmoQu4mc9gM', // Click en newsletter a socios (HR)
    membertype: 'donor',
    utm_source: ['newsletter-socios'],
    utm_medium: ['email']
}];


/**
 * Configure user action related labels
 */
spanishRemarketing.configUserActionLabels = [{
    label: 'Pqa7CIX_pWoQu4mc9gM', // Firma de petición general (HR)
    action: ['signup']
}, {
    label: 'Pqa7CIX_pWoQu4mc9gM', // Firma de petición general (HR)
    action: ['signup-artico']
}, {
    label: 'Pqa7CIX_pWoQu4mc9gM', // Firma de petición general (HR)
    action: ['signup-abejas']
}, {
    label: 'Gzn6CJrktmoQu4mc9gM', // Se hace socio online (HR)
    action: ['regular-donation']
}, {
    label: 'GWb8CNHltmoQu4mc9gM', // Hace una donación online (HR)
    action: ['one-donation']
}];
