/* global require */

// http://gulpjs.com/
// https://www.npmjs.com/package/gulp-concat
// https://www.npmjs.com/package/gulp-uglify
// https://www.npmjs.com/package/gulp-rename

var gulp = require('gulp'); 
var concat = require('gulp-concat'); 
var uglify = require('gulp-uglify'); 
var rename = require("gulp-rename"); 

gulp.task('scripts', function() {
  return gulp.src(['polyfill.js', 'hyperRemarketing.js', 'spanishRemarketing.js'])
    .pipe(concat('remarketing.js'))
  	.pipe(uglify())
  	.pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest('./'));
}); 

gulp.task('default', [ 'scripts']);

