/**
 * Object.create polyfill
 */
if (typeof Object.create != 'function') {
  Object.create = (function(undefined) {
    var Temp = function() {};
    return function (prototype, propertiesObject) {
      if(prototype !== Object(prototype) && prototype !== null) {
        throw TypeError('Argument must be an object, or null');
      }
      Temp.prototype = prototype || {};
      var result = new Temp();
      Temp.prototype = null;
      if (propertiesObject !== undefined) {
        Object.defineProperties(result, propertiesObject); 
      } 
      
      // to imitate the case of Object.create(null)
      if(prototype === null) {
         result.__proto__ = null;
      } 
      return result;
    };
  })();
}

/**
 * Date in yyyymmdd format
 */
if ( typeof (  Date.prototype.yyyymmdd ) === 'undefined' ) {
	
	Date.prototype.yyyymmdd = function() {
		var yyyy = this.getFullYear();
		var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1); // getMonth() is zero-based
		var dd  = this.getDate() < 10 ? "0" + this.getDate() : this.getDate();
		return "".concat(yyyy).concat(mm).concat(dd);
	};
	
}

/**
 * Get uniques from an Array 
 */
if ( typeof ([].getUnique) === 'undefined' ) {
	Array.prototype.getUnique = function(){
	   var u = {}, a = [];
	   for(var i = 0, l = this.length; i < l; ++i){
		  if(u.hasOwnProperty(this[i])) {
			 continue;
		  }
		  a.push(this[i]);
		  u[this[i]] = 1;
	   }
	   return a;
	};
}


