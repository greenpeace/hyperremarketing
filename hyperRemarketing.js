/* jshint browser:true */
/* global jQuery */

var hyperRemarketing = Object.create(null);

hyperRemarketing.GoogleConversionId = 0; // Configurable remarketing ID (number)

hyperRemarketing.GoogleOtherContentLabel = ''; // Configurable remarketing default content label (string)

hyperRemarketing.GoogleOtherActionLabel = ''; // Default user action, if it's not defined in configUserActionLabels

hyperRemarketing.version = '1.0.1';

/**
 * Configure the content related labels
 */
hyperRemarketing.configContentTagsLabels = [{
    label: '',
    pageTags: ['']
}, {
    label: '',
    pageTags: ['', '', '']
}];


/**
 * Configure the traffic source related labels
 */
hyperRemarketing.configTrafficSourceLabels = [{
    label: '',
    utm_source: ['newsletter-leads'],
    utm_medium: ['email']
}, {
    label: '',
    membertype: 'donor',
    utm_source: ['newsletter-socios'],
    utm_medium: ['email']
}];


/**
 * Configure user action related labels
 */
hyperRemarketing.configUserActionLabels = [{
    label: '',
    action: ['signup']
}];


/**
 * Prevent errors
 */
if (typeof window.google_tag_params === 'undefined') {
    window.google_tag_params = {};
}


/**
 * Count days with session and record last visit
 */
hyperRemarketing.addToDaysWithSession = function() {
    var date = new Date();
    /* Only if the browser supports localStorage, use Modernizr? */
    if (typeof localStorage === 'object') {
        if (localStorage.getItem('last_visit') !== null && localStorage.getItem('days_with_session') !== null) {
            if (localStorage.getItem('last_visit') != date.yyyymmdd()) {
                var days_with_session = Number(localStorage.getItem('days_with_session'));
                days_with_session++;
                localStorage.setItem('days_with_session', days_with_session.toString());
            }
        } else {
            localStorage.setItem('days_with_session', '1');
        }
        localStorage.setItem('last_visit', date.yyyymmdd());
    }
};


/**
 * Add days with session and last visit to Google Adwords Params
 */
hyperRemarketing.tagDaysWithSession = function() {
    if (typeof localStorage === 'object' && localStorage.getItem('days_with_session') !== null && localStorage.getItem('last_visit') !== null) {
        window.google_tag_params.dayswithsession = Number(localStorage.getItem('days_with_session'));
        window.google_tag_params.lastvisit = Number(localStorage.getItem('last_visit'));
    }
};


/**
 * Saves window.location.search in Session Storage to future use
 * @returns {boolean} True if it saved, false if it didn't
 */
hyperRemarketing.saveLastSourceStringSession = function() {
    if (typeof window.sessionStorage === 'object' && window.location.search !== '' && window.location.hash.indexOf('notrack') < 0) {
        sessionStorage.setItem('last_source_string', window.location.search);
        return true;
    } else {
        return false;
    }
};


/**
 * Gets the last source string either from the URL or session storage
 * @returns {string} Last source string
 */
hyperRemarketing.getLastSourceString = function() {
    if (window.location.search !== '' && window.location.hash.indexOf('notrack') < 0) {
        return window.location.search;
    } else if (typeof window.sessionStorage == 'object' && sessionStorage.getItem('last_source_string') !== null) {
        return sessionStorage.getItem('last_source_string');
    } else {
        return '';
    }
};


/**
 * Parse last_source_string. If it includes Google's Adwords gclid : utm_medium = cpc and utm_source = google
 * @param   {string} last_source_string String to parse, starting with ?
 * @returns {object} Parameters and values
 */
hyperRemarketing.getJsonFromString = function(last_source_string) {
    var search = last_source_string.substring(1);
    var result = search ? JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
        function(key, value) {
            return key === "" ? value : decodeURIComponent(value);
        }) : {};
    if (result.gclid) {
        result.utm_medium = "cpc";
        result.utm_source = "google";
    }
    return result;
};


/**
 * If a list of tags was declared as the array window.google_tag_params.pageTags returns window.google_tag_params.pageTags else searches in the DOM and adds them to window.google_tag_params.pageTags.
 * 
 * @returns {Array} List ot tags
 */
hyperRemarketing.getContentTags = function() {

    if (jQuery.isArray(window.google_tag_params.pageTags) && window.google_tag_params.pageTags.length >= 1) {
        return window.google_tag_params.pageTags;
    } else if (jQuery('.tags a').length > 0) {
        window.google_tag_params.pageTags = [];
        jQuery('.tags a').each(function(index, value) {
            var tagText = jQuery(value).text();
            window.google_tag_params.pageTags.push(tagText);
        });
        return window.google_tag_params.pageTags;
    } else {
        window.google_tag_params.pageTags = [];
        return window.google_tag_params.pageTags;
    }
};


/**
 * Searches the label for a specific utm_source and utm_medium
 * Populates window.google_tag_params.membertype with configTrafficSourceLabels membertype
 * @param   {string} utm_source UTM source as defined by the URL Builder
 * @param   {string} utm_medium UTM medium as defined by the URL builder
 * @returns {string} Returns the label correspondant to the source
 */
hyperRemarketing.getSourceTag = function(utm_source, utm_medium) {
    var sourceTag = false;
    var membertype = '';
    jQuery.each(this.configTrafficSourceLabels, function(index, value) {
        if (value.utm_source.indexOf(utm_source) >= 0 && value.utm_medium.indexOf(utm_medium) >= 0) {
            if (typeof value.label === 'string') {
                sourceTag = value.label;
            }
            if (typeof value.membertype === 'string') {
                membertype = value.membertype;
            }
        }
    });
    if (membertype !== '') {
        window.google_tag_params.membertype = membertype;
    }
    return sourceTag;
};

/**
 * Searches the label for aspecific action
 * Populates window.google_tag_params.useraction with the action name
 * @param   {string} action Action name (For example 'signup')
 * @returns {string} Returns the label correspondant to the action, returns the default action label if the action has no labels
 */
hyperRemarketing.getUserActionTag = function(action) {
    var actionTag = this.GoogleOtherActionLabel;
    jQuery.each(this.configUserActionLabels, function(index, value) {
        if (value.action.indexOf(action) >= 0) {
            if (typeof value.label === 'string') {
                actionTag = value.label;
            }
        }
    });

    window.google_tag_params.useraction = action;
    var actionTagArray = [actionTag];
    return actionTagArray;
};


/**
 * Searches the google remarketing label to a specific content tag 
 * @param   {string} contentTag Content tag
 * @returns {string} Returns Google remarketing label
 */
hyperRemarketing.getLabelFromTag = function(contentTag) {
    var returnLabel = false;
    jQuery.each(this.configContentTagsLabels, function(index, value) {
        jQuery.each(value.pageTags, function(indextag, valuetag) {
            if (valuetag === contentTag) {
                returnLabel = value.label;
            }
        });
    });
    return returnLabel;
};


/**
 * Send remarketing info to Google.
 * @param {array} remarketingLabels Array of remarketing labels
 */
hyperRemarketing.sendToGoogle = function(remarketingLabels) {
    var self = this;
    jQuery.getScript('https://www.googleadservices.com/pagead/conversion_async.js').done(function(script, textStatus) {
        for (var i in remarketingLabels) {
            if (remarketingLabels.hasOwnProperty(i)) {
                window.google_trackConversion({
                    google_conversion_id: self.GoogleConversionId,
                    google_conversion_label: remarketingLabels[i],
                    google_custom_params: window.google_tag_params,
                    google_remarketing_only: true
                });
            } // endif
        } // endfor
    }); // end GetScript
};


/**
 *
 * ****** Main tracking method. Starts hypermarketing ******
 * 
 */
hyperRemarketing.track = function() {

    // DAYS WITH SESSION
    this.addToDaysWithSession();
    this.tagDaysWithSession();

    // SOURCE, DONNOR, LEAD
    this.saveLastSourceStringSession();
    var last_source_string = this.getLastSourceString();
    var trafficSource = this.getJsonFromString(last_source_string);
    var sourceLabel = this.getSourceTag(trafficSource.utm_source, trafficSource.utm_medium);

    // CONTENT TAGS AND LABELS
    var pageTags = this.getContentTags();
    var listOfContentLabels = [];
    if (jQuery.isArray(pageTags) && pageTags.length >= 1) {
        var lft;
        for (var value in pageTags) {
            if (pageTags.hasOwnProperty(value)) {
                lft = this.getLabelFromTag(pageTags[value]);
                if (lft !== false) {
                    listOfContentLabels.push(lft);
                }
            }
        }
    }
    var uniqueContentLabels = listOfContentLabels.getUnique();

    // PUT ALL LABELS IN AN ARRAY AND SEND IT TO GOOGLE
    var allLabels = [];
    if (sourceLabel !== false) {
        allLabels.push(sourceLabel);
    }
    if (uniqueContentLabels.length === 2 || uniqueContentLabels.length === 1) {
        allLabels = allLabels.concat(uniqueContentLabels);
    } else {
        allLabels = allLabels.concat(this.GoogleOtherContentLabel);
    }
    this.sendToGoogle(allLabels);
};


/**
 * Tracks a user action like signup
 * @param {string} action User action, as defined in configUserActionLabels
 */
hyperRemarketing.trackUserAction = function(action) {
    var userActionLabel = this.getUserActionTag(action);
    this.sendToGoogle(userActionLabel);
};
